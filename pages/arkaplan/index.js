import React from 'react'
import {useRouter}  from "next/router"
const index = () => {
    const router = useRouter()
    const {color} = router.query
  console.log(color)
  return (
    <div style={{backgroundColor:color, height:"1000px"}}>Arka Plan rengi : {color}</div>
  )
}

export default index