import React from "react";

export const index = (props) => {
  console.log({ props });
  return (
    <div>
With Server side rendering
      <ul>
        {props.countries.map((country, key) => {
          return <li key={country.name.common}>{country.name.common}</li>;
        })}
      </ul>
    </div>
  );
};

export const getServerSideProps = async () => {
  const countries = await fetch("https://restcountries.com/v3.1/all").then(
    (res) => res.json()
  );
  return {
    props: {
      countries,
    },
  };
};
export default index;
