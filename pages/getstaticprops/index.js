import React from 'react'

const index = (props) => {
  return (
    <div>
          <ul>
        {props.countries.map((country, key) => {
          return <li key={country.name.common}>{country.name.common}</li>;
        })}
      </ul>
    </div>
  )
}

export default index

export const  getStaticProps = async()=>{
    const countries = await fetch("https://restcountries.com/v3.1/all").then(
        (res) => res.json()
      );
      return {
        props: {
          countries,
        },
      };
}