import React, { useState } from "react";
import { useEffect } from "react";
const index = () => {
  const [countriesState, setCountriesState] = useState();

  const getCountries = async () => {
    fetch("https://restcountries.com/v3.1/all").then((res) => res.json()).then((resp)=>{
    console.log(resp)  
    setCountriesState(resp)});
      
  
  };
  useEffect(() => {
   getCountries();
  },[]);
   console.log(countriesState);
  return (
    <div>
     With useEffect
      <ul>
    {countriesState&& ( countriesState.map((country, key) => {
          return <li key={country.name.common}>{country.name.common}</li>;
        }))
      }

    </ul>
    </div>
  );
};

export default index;
